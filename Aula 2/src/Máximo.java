import java.util.Scanner;

public class M�ximo {
	
	private static Scanner entrada;
	
	public static void main(String[] args) {
		
		entrada = new Scanner(System.in);
		System.out.println();
		System.out.println("Programa para determinar o m�ximo de 3 valores" );
		System.out.println("" );
		
		System.out.println("1� N�mero?");
		int numero1;
		
		numero1 = entrada.nextInt();
		System.out.println("2� N�mero?");
		int numero2;
		
		numero2 = entrada.nextInt();
		System.out.println("3� N�mero?");
		int numero3;
		
		numero3 = entrada.nextInt();
		
		int maximo = 0;
		if (numero1 > numero2) {
			if (numero1 > numero3) {
				maximo = numero1;
			}
		} else if (numero2 > numero3) {
			maximo = numero2;
		} else
			maximo = numero3;
		System.out.println("O n�mero maior � o " + maximo);
	}

}
